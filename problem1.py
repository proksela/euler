# If we list all the natural numbers below 10 that are multiples
# of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.


def fun(max):
    # funkcja, wykonuje operacje, może przyjmować argumenty, może zwracać wartość
    sum = 0     # deklaracja zmiennej
    for i in range(max):        # pętla po wartościach 0, 1, ... max - 1
        if i % 3 == 0 or i % 5 == 0:    # warunek na operacji modulo – reszta z dzielenia
            sum = sum + i       # przypisanie nowej wartości zmiennej
    return sum


print(fun(1000))


