# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?


def pri(x):
    for d in range(x-2):        # pętla po wartościach 0, 1, ... x - 3
        i = d + 2               # dzięki temu w pętli korzystamy z wartości 2, 3, .... x - 2, x - 1
        if x % i == 0:
            return False
    return True
    # funkcja może zwracać liczby wprost `return 5`,
    # wartości logiczne, `return True` lub `return False`
    # wartości zmiennych `return x`, gdzie do x może być przypisana dowolna wartość, np. 5 albo True

print(pri(10))      # to nie jest jeszcze rozwiązanie, na razie sprawdza tylko czy dana liczba jest pierwsza
